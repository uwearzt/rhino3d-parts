import Rhino as rh
import rhinoscriptsyntax as rs
import scriptcontext as sc

x_length = 168
y_length = 305

def check_params():
    x_nr = x_length / 42
    y_nr = y_length / 42

    print("x_nr: " + x_nr + " y_nr: " + y_nr)


def test():

    a = rs.CreatePoint(0.0, 0.0, 0.0)
    b = rs.CreatePoint(20.0, 20.0, 0.0)

    print(b)

    c = rs.AddLine(a, b)

if( __name__ == "__main__" ):
    check_params()
    test()