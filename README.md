# Rhino 3D stuff

My Rhino 3D drawings and scripts not related to a single project.

## License

GNU General Public License, Version 3.0 [LICENSE](LICENSE) or
[https://spdx.org/licenses/GPL-3.0-or-later.html](https://spdx.org/licenses/GPL-3.0-or-later.html)